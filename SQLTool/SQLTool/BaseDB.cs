﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.Dynamic;
using System.Data;

namespace SQLTool
{
    public class BaseDB : IDisposable
    {
        private string ServerName;
        private string UserAccount;
        private string UserPassword;
        private string DataBaseName;

        private SqlConnection connection = new SqlConnection();

        public BaseDB()
        {
            string constr = ConfigurationManager.AppSettings["db"];
            if (constr == null)
            {
                throw new Exception("請在Appconfig加入connection string");
            }
            else
            {
                connection = new SqlConnection(constr);
                connection.Open();
            }
        }

        public BaseDB(string ServerName, string UserAccount, string UserPassword, string DataBaseName)
        {
            this.ServerName = ServerName;
            this.UserAccount = UserAccount;
            this.UserPassword = UserPassword;
            this.DataBaseName = DataBaseName;
            string connectionString = $@"Persist Security Info=False;
                                                          User ID={UserAccount}; 
                                                          Password={UserPassword}; 
                                                          Initial Catalog={DataBaseName}; 
                                                          Server={ServerName}";
            this.connection.ConnectionString = connectionString;
            connection.Open();

        }

        public List<T> NewQuery<T>(String sqlstring, params object[] data) where T : new() //看成兩個參數域<型別參數>(一般參數) // params類似Console.WriteLine("{0}, {1}, param1, param2");
        {
            List<T> list = new List<T>(); //目標列表

            for (int i = 0; i < data.Length; i++)
            {
                if (sqlstring.Contains("{" + i + "}"))
                {
                    sqlstring = sqlstring.Replace("{" + i + "}", data[i].ToString());
                }
            }

            SqlCommand command = new SqlCommand(sqlstring, connection);


            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                T t = new T(); //傳進類別(e.g. SQLListModel)

                PropertyInfo[] info = t.GetType().GetProperties(); //取得t底下所有屬性(包括資料型別、名稱、值...)             

                //以Model為主 所以數量要和資料庫完全相同
                //for (int i = 0; i < info.Length; i++) //賦予t值(e.g. SQLListModel.Title = ~ --> t底下的屬性)
                //{
                //    // dynamic change type
                //    info[i].SetValue(t, Convert.ChangeType(reader[info[i].Name], info[i].PropertyType)); //將撈回的資料做轉型把值丟給t
                //    //e.g. t的型別(所屬類別)為SQLModel (屬性有Title Name ...)
                //    //     info[i] = Title屬性(包含資料型別、名稱、值...)  ==>  info[i].SetValue是將t內的Title屬性賦予值 由info[i]來確認要傳入的值要給的屬性   


                //    //info[i].SetValue(t, reader[info[i].Name]);

                //}

                for (int i = 0; i < reader.FieldCount; i++) //以資料庫為主 有對應到再賦予值 沒有的就null
                {
                    foreach (var prop in info)
                    {
                        if (prop.Name.Equals(reader.GetName(i))) //看名稱有沒有一樣(對應到)
                        {
                            prop.SetValue(t, Convert.ChangeType(reader[i], prop.PropertyType));
                        }
                    }
                }
                list.Add(t);
            }
            reader.Close();
            return list;
        }

        public List<T> NewQuery<T>(string sqlstring, Dictionary<string, object> dict = null) where T : new()
        {
            List<T> list = new List<T>(); //目標列表

            // account = 1' or 1=1 --
            // select * from User where account = @account;   

            SqlCommand command = new SqlCommand(sqlstring, connection);

            if (dict != null)
            {
                foreach (var data in dict)
                {
                    if (sqlstring.Contains(data.Key))
                    {
                        string key = data.Key;
                        command.Parameters.AddWithValue(key, dict[key].ToString());
                        //Console.WriteLine(dict[key]);
                    }
                }
            }

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                T t = new T(); //傳進類別(e.g. SQLListModel)

                PropertyInfo[] info = t.GetType().GetProperties(); //取得t底下所有屬性             
                for (int i = 0; i < reader.FieldCount; i++) //以讀到的資料為主 有對應到再賦予值 沒有的就null
                {
                    foreach (var prop in info)
                    {
                        if (prop.Name.Equals(reader.GetName(i)))
                        {
                            var temp = reader[i];
                            Console.WriteLine(temp);
                            if (temp == Convert.DBNull && prop.PropertyType.Equals(typeof(int)))
                            {

                                prop.SetValue(t, Convert.ChangeType(0, prop.PropertyType));
                                break;
                            }
                            else
                            {
                                prop.SetValue(t, Convert.ChangeType(reader[i], prop.PropertyType));
                                break;
                            }
                        }
                    }
                }
                list.Add(t);
            }
            reader.Close();
            return list;
        }

        public List<dynamic> NewQuery(String sqlstring)
        {
            SqlCommand command = new SqlCommand(sqlstring, connection);
            SqlDataReader reader = command.ExecuteReader();

            List<dynamic> list = new List<dynamic>();  //dynamic 編譯時(compiling)不去確認型別 在執行(runtime)時在自動帶入     
            while (reader.Read())
            {
                var temporclass = new ExpandoObject() as IDictionary<string, object>;
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    temporclass.Add(reader.GetName(i), reader[i]);
                }
                list.Add(temporclass);
            }
            reader.Close();
            return list;
        }

        public int NewInsert(string sqlstr, Dictionary<string, object> dict)
        {
            SqlCommand command = new SqlCommand(sqlstr, connection);

            if (dict != null)
            {
                foreach (var data in dict)
                {
                    if (sqlstr.Contains(data.Key))
                    {
                        string key = data.Key;
                        command.Parameters.AddWithValue(key, dict[key].ToString());
                        //Console.WriteLine(dict[key]);
                    }
                }
            }

            int reader = command.ExecuteNonQuery();
            return reader;
        }

        public void Insert<T>(T t) where T : new()
        {
            //撈取PK
            //由Schmea中找
            string sqlstr = $@"SELECT column_name as PK
                              FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
                              INNER JOIN
                                  INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
                                        ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
                                           TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME AND
                                           KU.table_name = '{t.GetType().Name}'
                              ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;";
            SqlCommand sqlCm = new SqlCommand(sqlstr, connection);
            SqlDataReader reader = sqlCm.ExecuteReader();
            reader.Read();
            string PK = reader["PK"].ToString();
            //Console.WriteLine(PK);
            reader.Close();

            PropertyInfo[] info = t.GetType().GetProperties();

            string tablename = t.GetType().Name;
            DataTable table = connection.GetSchema("Columns", new[] { "PTT_System", null, tablename });
            //schema 資料表結構           

            List<(String, String)> list = new List<(String, String)>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                list.Add((table.Rows[i].Field<string>(3), table.Rows[i].Field<string>(6)));
            }

            list = list.Where(x => x.Item2.Equals("NO")).ToList();
            list = list.Where(x => !x.Item1.Equals(PK)).ToList();

            Dictionary<string, object> user_insert = new Dictionary<string, object>();

            StringBuilder keys = new StringBuilder();
            StringBuilder values = new StringBuilder();
            foreach (var p in info)
            {
                if (!p.Name.Equals(PK))
                {
                    user_insert.Add(p.Name, p.GetValue(t));
                    //組sqlstring                         
                    keys.Append("," + p.Name);
                    values.Append("," + "'" + p.GetValue(t) + "'");
                }
            }
            keys.Remove(0, 1); //去除逗號         
            values.Remove(0, 1); //去除逗號

            foreach (var item in list)
            {
                if (!user_insert.Keys.Contains(item.Item1) || string.IsNullOrEmpty(user_insert[item.Item1].ToString()))
                {
                    throw new Exception("It's no null! the row is" + item.Item1);
                }
            }
            //Console.WriteLine(keys.ToString());
            //Console.WriteLine(values.ToString());

            string sql = $@"insert into {tablename}({keys.ToString()})
                            values({values.ToString()})";

            //Console.WriteLine(sql);
            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
        }

        public void Update<T>(T t) where T : new()
        {
            //UPDATE "表格"
            //SET "欄位1" = [值1], "欄位2" = [值2]
            //WHERE "條件"; --> 用ID(PK)

            //撈取PK
            string sqlstr = @"SELECT column_name as PK
                              FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
                              INNER JOIN
                                  INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
                                        ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
                                           TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME AND
                                           KU.table_name = 'PTTuser'
                              ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;";
            SqlCommand sqlCm = new SqlCommand(sqlstr, connection);
            SqlDataReader reader = sqlCm.ExecuteReader();
            reader.Read();
            string PK = reader["PK"].ToString();
            //Console.WriteLine(PK);
            reader.Close();

            PropertyInfo[] info = t.GetType().GetProperties();
            string tableNeme = t.GetType().Name;
            string valuePK = "";
            foreach (var p in info)
            {
                if (p.Name.Equals(PK))
                {
                    valuePK = p.GetValue(t).ToString();
                }
            }

            string setContent = "";
            foreach (var p in info)
            {
                if (!p.Name.Equals(PK))
                {
                    setContent += $",{p.Name} = '{p.GetValue(t)}'";
                }
            }
            setContent = setContent.TrimStart(',');

            string sql = $@"update {tableNeme}
                            set {setContent}
                            where {PK} = {valuePK}";

            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();

            //Console.WriteLine(setContent);
            //Console.WriteLine(sql);

        }

        public void Delete<T>(T t) where T : new()
        {
            //DELETE FROM "表格名"
            //WHERE "條件";

            //撈取PK
            string sqlstr = @"SELECT column_name as PK
                              FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
                              INNER JOIN
                                  INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
                                        ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND
                                           TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME AND
                                           KU.table_name = 'PTTuser'
                              ORDER BY KU.TABLE_NAME, KU.ORDINAL_POSITION;";
            SqlCommand sqlCm = new SqlCommand(sqlstr, connection);
            SqlDataReader reader = sqlCm.ExecuteReader();
            reader.Read();
            string PK = reader["PK"].ToString();
            //Console.WriteLine(PK);
            reader.Close();

            PropertyInfo[] info = t.GetType().GetProperties();
            string tableNeme = t.GetType().Name;
            string valuePK = "";
            foreach (var p in info)
            {
                if (p.Name.Equals(PK))
                {
                    valuePK = p.GetValue(t).ToString();
                }
            }

            string sql = $@"delete from {tableNeme}
                            where {PK}={valuePK}";

            SqlCommand command = new SqlCommand(sql, connection);
            command.ExecuteNonQuery();
        }


        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
